import {BASE_URL} from '@/services/api'
import {METHOD, request} from '@/utils/request'

export async function queryAllField(payload) {
  return request(`${BASE_URL}/field`, METHOD.GET, payload)
}

export async function addField(payload) {
  return request(`${BASE_URL}/field`, METHOD.POST, payload)
}

export async function saveBatch(payload) {
  return request(`${BASE_URL}/field/saveBatch`, METHOD.POST, payload)
}

export async function deleteById(payload) {
  return request(`${BASE_URL}/field/deleteById/${payload}`, METHOD.DELETE)
}

export async function update(payload) {
  return request(`${BASE_URL}/field`, METHOD.PUT, payload)
}
