import {addActivation, deleteById, disable, queryAllActivation, update} from '@/services/systemManage/activation'
import {storePrompt} from '@/store/modules/commonStore'

export default {
  namespaced: true,
  state: {
    tableData: [],
    showTableDada: [],
    currentKey: undefined,
    popupVisible: false,
    popupTitle: undefined,
    managePopupVisible: false,
    currentEditKey: undefined,
    editData: null
  },
  actions: {
    async queryAllActivation({commit, state}, payload) {
      await queryAllActivation({...payload, modelId: state.currentKey}).then((obj) => {
        let {data, success} = obj.data
        if (success) {
          commit("setState", {name: 'tableData', value: data})
          commit("setState", {name: 'showTableDada', value: data})
        }
      })
    },
    async deleteById({commit, state}, payload) {
      await deleteById(payload).then((obj) => {
        storePrompt(obj.data)
      })
    },
    async disable({commit, state, dispatch}, payload) {
      await disable(payload).then(() => {
        dispatch('queryAllActivation')
      })
    },
    async update({commit, state}, payload) {
      await update({...payload, objectId: state.currentEditKey, modelId: state.currentKey}).then((obj) => {
        storePrompt(obj.data)
      })
    },
    async addActivation({commit, state}, payload) {
      await addActivation({...payload, modelId: state.currentKey}).then((obj) => {
        storePrompt(obj.data)
      })
    },
  }
}
