import {addModel, deleteModel, queryModelList, rename, update} from '@/services/systemManage/modelManage'
import {saveBatch} from '@/services/systemManage/fieldManage'
import {storePrompt} from '@/store/modules/commonStore'

export default {
  namespaced: true,
  state: {
    modelList: [],
    pagination: {},
    queryForm: {},
    modalVisible: false,
    mustVisible: false,
    modalTitle: '',
    currentKey: undefined
  },
  actions: {
    async queryModelList({commit, state}, payload) {
      await queryModelList({...payload, ...state.queryForm}).then((obj) => {
        let {data, success} = obj.data
        if (success) {
          let {total, current, records} = data
          commit('setState', {
            name: 'pagination',
            value: {...state.pagination, total: Number(total), current: Number(current)}
          })
          commit("setState", {name: 'modelList', value: records})
        }
      })
    },
    async addModel({commit, state}, payload) {
      await addModel(payload).then((obj) => {
        let {data} = obj
        storePrompt(data)
      })
    },
    async update({commit, state}, payload) {
      await update(payload).then((obj) => {
        let {data} = obj
        storePrompt(data)
      })
    },
    async deleteModel({commit, state}, payload) {
      await deleteModel(payload).then((obj) => {
        let {data} = obj
        storePrompt(data)
      })
    },
    async rename({commit, state}, payload) {
      await rename({...payload, objectId: state.currentKey}).then((obj) => {
        let {data} = obj
        storePrompt(data)
      })
    },
    async saveBatchField({commit, state}, payload) {
      await saveBatch(payload).then((obj) => {
        let {data} = obj
        storePrompt(data)
      })
    }
  }
}
