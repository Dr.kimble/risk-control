package com.wjz.engine.impl;

import com.wjz.engine.model.AntiFraudEngine;

import java.util.Map;

/**
 * @author wjz
 * @date 2021/9/12 20:02
 */
public class AntiFraudEngineImpl implements AntiFraudEngine {
    @Override
    public Map<String, Object> executeAbstraction(Long modelId, Map<String, Map<String, ?>> data) {
        return null;
    }

    @Override
    public Map<String, Object> executeActivation(Long modelId, Map<String, Map<String, ?>> data) {
        return null;
    }
}
