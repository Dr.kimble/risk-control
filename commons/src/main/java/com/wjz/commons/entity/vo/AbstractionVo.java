package com.wjz.commons.entity.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author wjz
 * @date 2021/9/5 21:49
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "AbstractionVo", description = "抽象实体")
public class AbstractionVo implements Serializable {

    @TableId
    private Long objectId;

    private String name;

    private String label;

    private Long modelId;

    private Integer aggregateType;

    private String searchField;

    private Integer searchIntervalType;

    private Integer searchIntervalValue;

    private String functionField;

    private String ruleScript;

    private Integer status;

    private String comment;

    private Date createTime;

    private Date updateTime;

    private String ruleDefinition;
}
