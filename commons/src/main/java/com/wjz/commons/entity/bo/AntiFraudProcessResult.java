package com.wjz.commons.entity.bo;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author wjz
 * @date 2021/9/12 19:39
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "AntiFraudProcessResult", description = "反欺诈分析结果")
public class AntiFraudProcessResult {

    private Map<String, ?> abstractions;

    private Map<String, ?> adaptations;

    private Map<String, ?> activations;

    private Map<String, List<HitObject>> hitsDetail = new HashMap<>();

    private Map<String, Object> respTimes = new HashMap<>();
}
